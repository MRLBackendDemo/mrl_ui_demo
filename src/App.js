import React, { Component } from 'react';
import Alert from 'react-s-alert';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import 'react-s-alert/dist/s-alert-css-effects/scale.css';
import 'react-s-alert/dist/s-alert-css-effects/bouncyflip.css';
import 'react-s-alert/dist/s-alert-css-effects/flip.css';
import 'react-s-alert/dist/s-alert-css-effects/genie.css';
import 'react-s-alert/dist/s-alert-css-effects/jelly.css';
import 'react-s-alert/dist/s-alert-css-effects/stackslide.css';

const loading = () => <div className=""></div>;

//Container
const DashboardLayout = React.lazy(() => import('./container/DashboardLayout'));

//Pages
const Login = React.lazy(() => import('./components/login'));
const Error_404 = React.lazy(() => import('./components/404'));

class App extends Component{
  render(){
  return (
    <div className="App">
      <BrowserRouter> 
        <React.Suspense fallback={loading()}>
          <Switch>
            <Route exact path='/404' component={Error_404} />
            <Route exact path='/login' component={Login} />            
            <Route path="/" name="Layout" render={props => <DashboardLayout {...props} />} />            
          </Switch>  
        </React.Suspense>  
      </BrowserRouter>
      <Alert stack={{limit: 1}} />
    </div>
  );
  }
}

export default App;

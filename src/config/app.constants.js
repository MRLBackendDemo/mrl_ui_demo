export const BASE_URL = "http://localhost:8080";

export const GLOBAL_API = {
  LOGIN: `${BASE_URL}/api/v1/login/user`,
  ROLELIST: `${BASE_URL}/api/v1/get/roleList`,
  ADDUPDATEROLE: `${BASE_URL}/api/v1/addUpdateRole`,
  USERLIST: `${BASE_URL}/api/v1/get/userList`,
  ADDUPDATEUSER: `${BASE_URL}/api/v1/addUpdateUser`,
  PERMISSIONLIST: `${BASE_URL}/api/v1/get/permissionList`,
  UPDATEPERMISSION: `${BASE_URL}/api/v1/update/permissionList`,
};

export const GLOBAL_VARIABLE = {
  ROLE_ID: "roleIds",
};

export const CONFIG = {
  HEADERS: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json",
  },
};

export const utility = {
    capFstLetr,
    lowFstLetr,
    uniqueArray,
    insertAfter,
    clearUiTextContent,
    dynamicButton,
    updateItemInArrObj
}

function capFstLetr(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function lowFstLetr(string) {
    return string.replace(/\S*/g, function (word) {
        return word.charAt(0) + word.slice(1).toLowerCase();
    });
}

function uniqueArray(array, propertyName) {
    return array.filter((e, i) => array.findIndex(a => a[propertyName] === e[propertyName]) === i);
 }

function insertAfter(referenceNode, newNode) {
     referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function clearUiTextContent(classname){
    const clrMess = document.querySelectorAll(`.${classname}`);
    if(clrMess){clrMess.forEach(itm=>{
        itm.textContent='';
    })};
}

//dynamic button
function dynamicButton(clss,atrib,otrElme,btnName){
   
    const dButton = document.createElement('button');
    clss.forEach(ele => {
        dButton.classList.add(ele);
    });
    atrib.forEach(ele => {
        dButton.setAttribute(ele.name,ele.val);
    });
    
    let tmp= dButton.textContent=btnName;  
    dButton.innerHTML =otrElme+tmp;

    return dButton;
}

function updateItemInArrObj(id,ths,arrayName,objName,itemAttributes){
    let vals = ths.state;
    let index = vals[arrayName].findIndex((obj => obj[objName] === id));
    console.log(index)
      ths.setState({
        [arrayName]: [
           ...vals[arrayName].slice(0,index),
           Object.assign({}, vals[arrayName][index], itemAttributes),
           ...vals[arrayName].slice(index+1)
        ]
      });
  }
import axios from 'axios';
import {GLOBAL_API,GLOBAL_VARIABLE} from '../../config/app.constants';
export const commonService = {
    getRoleList,
    getUserList,
    addRole,
    addUser,
    updateUser,
    getPermissionList,
    updatePermissionList,
    updateRole
}

async function  getRoleList() {
    return await axios.get(GLOBAL_API.ROLELIST);
}

async function  addRole(createDataTime,roleName,status) {
    return await axios.post(GLOBAL_API.ADDUPDATEROLE,{createDataTime,roleName,status});
}

async function updateRole(roleId,createDataTime,roleName,status){
    return await axios.put(GLOBAL_API.ADDUPDATEROLE,{roleId,createDataTime,roleName,status});
}

async function  getUserList() {
    return await axios.get(GLOBAL_API.USERLIST);
}

async function  addUser(createDateTime,status,userEmail,userPassword) {
    return await axios.post(GLOBAL_API.ADDUPDATEUSER,{createDateTime,status,userEmail,userPassword});
}

async function  updateUser(userId,createDateTime,status,userEmail,userPassword) {
    return await axios.put(`${GLOBAL_API.ADDUPDATEUSER}`,{userId,createDateTime,status,userEmail,userPassword});
}

async function  getPermissionList(roles) {
    return await axios.get(`${GLOBAL_API.PERMISSIONLIST}?${GLOBAL_VARIABLE.ROLE_ID}=${roles}`);
}

async function  updatePermissionList(id,status) {
    return await axios.put(`${GLOBAL_API.UPDATEPERMISSION}/${id}`,{status});
}
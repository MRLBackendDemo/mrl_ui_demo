import { GLOBAL_VARIABLE } from '../../config/app.constants';

class StorageService {

    getRoleIds() {
        return localStorage.getItem(GLOBAL_VARIABLE.ROLE_ID);
    }

    setRoleIds(role) {
        localStorage.setItem(GLOBAL_VARIABLE.ROLE_ID, role);
    }
}

export default StorageService;
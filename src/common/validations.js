import { Alert } from '../common/service/AlertService';
import { utility } from '../common/utility';

export const validations = {
    loginValidate,
    validateEmail,
    addUserValidate,
    addRoleValidate,
    formUiValidate,
    updateUserValidate,
    updateRoleValidate
}

function loginValidate(val) {
    if (!val.email) {
        Alert.alertBar('Email ID is required.','bg-red');
    }
    else if (!val.pass) {
        Alert.alertBar('Password is required.','bg-red');
    }
    else {
        return true;
    }
}

function addUserValidate(vals) {
    let error=[];    
    if(!vals.userEmail){
        error=[...error,{key: 'userEmail', set: 'N',message: 'Email required.'}];
    }else if(!validateEmail(vals.userEmail)){
        error=[...error,{key: 'userEmail', set: 'N',message: 'Email ID format is wrong.'}];        
    }else{
        error=[...error,{key: 'userEmail', set: 'Y'}];
    }
    
    (!vals.status || vals.status==='Select') ?
        error=[...error,{key:'status', set: 'N', message: 'Please select a status.'}] :
        error=[...error,{key:'status', set: 'Y'}];
    
    (!vals.createDateTime) ?
        error=[...error,{key:'createDateTime', set: 'N', message: 'Please pick a date.'}] :
        error=[...error,{key:'createDateTime', set: 'Y'}];
    
    (!vals.userPassword) ?
        error=[...error,{key:'userPassword', set: 'N', message: 'Password required.'}] :
        error=[...error,{key:'userPassword', set: 'Y'}];
    //clear form or previous errors
    utility.clearUiTextContent('addUserValidate');       
    //assign errors
    return formUiValidate(error,'addUserValidate');    
}

function  updateUserValidate(vals) {
    let error=[];    
    if(!vals.editUserEmail){
        error=[...error,{key: 'editUserEmail', set: 'N',message: 'Email required.'}];
    }else if(!validateEmail(vals.editUserEmail)){
        error=[...error,{key: 'editUserEmail', set: 'N',message: 'Email ID format is wrong.'}];        
    }else{
        error=[...error,{key: 'editUserEmail', set: 'Y'}];
    }
    
    (!vals.editStatus || vals.editStatus==='Select') ?
        error=[...error,{key:'editStatus', set: 'N', message: 'Please select a status.'}] :
        error=[...error,{key:'editStatus', set: 'Y'}];
    
    (!vals.editCreateDateTime) ?
        error=[...error,{key:'editCreateDateTime', set: 'N', message: 'Please pick a date.'}] :
        error=[...error,{key:'editCreateDateTime', set: 'Y'}];
    
    (!vals.editUserPassword) ?
        error=[...error,{key:'editUserPassword', set: 'N', message: 'Password required.'}] :
        error=[...error,{key:'editUserPassword', set: 'Y'}];
    //clear form or previous errors
    utility.clearUiTextContent('updateUserValidate');       
    //assign errors
    return formUiValidate(error,'updateUserValidate'); 
}

function addRoleValidate(vals) {
    let error=[]; 
    (!vals.roleName) ?
        error=[...error,{key:'roleName', set: 'N',message:'Role name is required.'}] :
        error=[...error,{key:'roleName', set: 'Y'}];
    
    (!vals.status || vals.status==='Select') ?
        error=[...error,{key:'status', set: 'N',message:'Please select a status.'}] :
        error=[...error,{key:'status', set: 'Y'}];
    
    (!vals.createDateTime) ?
        error=[...error,{key:'createDateTime', set: 'N',message:'Please pick a date.'}] :
        error=[...error,{key:'createDateTime', set: 'Y'}];    
    //clear form or previous errors
    utility.clearUiTextContent('addRoleValidate');       
    //assign errors
    return formUiValidate(error,'addRoleValidate');
    
}

function updateRoleValidate(vals) {
    let error=[]; 
    (!vals.editRoleName) ?
        error=[...error,{key:'editRoleName', set: 'N',message:'Role name is required.'}] :
        error=[...error,{key:'editRoleName', set: 'Y'}];
    
    (!vals.editRoleStatus || vals.editRoleStatus==='Select') ?
        error=[...error,{key:'editRoleStatus', set: 'N',message:'Please select a status.'}] :
        error=[...error,{key:'editRoleStatus', set: 'Y'}];
    
    (!vals.editRoleDate) ?
        error=[...error,{key:'editRoleDate', set: 'N',message:'Please pick a date.'}] :
        error=[...error,{key:'editRoleDate', set: 'Y'}];    
    //clear form or previous errors
    utility.clearUiTextContent('updateRoleValidate');       
    //assign errors
    return formUiValidate(error,'updateRoleValidate');
}

function validateEmail(mail) {
    const pattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    if (pattern.test(mail)) {        
        return true;
    }
    return false;
}

function formUiValidate(error,classname){
    let errRtn;
    error.forEach(err=>{
        const elements = document.getElementById(err.key);
        const mess = document.createElement('span');
        mess.classList.add(classname);
        mess.classList.add('text-red');
        if(err.set==='N'){
            elements.classList.add('border-danger')
            mess.textContent=err.message;
            utility.insertAfter(elements,mess);
        }else{
            elements.classList.remove('border-danger');
        }
        if(errRtn!==false){
        err.message  ? errRtn=false : errRtn=true;
        }
    });
    return errRtn;
}
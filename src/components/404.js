import React from 'react';
import { Link } from 'react-router-dom';

const Error_404 =()=>{    
    return (
    <div className="bg-white">
    <div id="layoutError">
    <div id="layoutError_content">
        <main>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-lg-6">
                        <div className="text-center mt-4">
                            <p className="img-fluid p-4 error-404"  alt="" ></p>
                            <p className="lead">This requested URL was not found on this server.</p>
                            <Link className="text-arrow-icon" to="/dashboard">                               
                                <i className="ml-0 mr-1 fa fa-arrow-left" aria-hidden="true"></i>
                                Return to Dashboard
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <div id="layoutError_footer">
        <footer className="footer mt-auto footer-light">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-6 small">Copyright &#xA9; More Retail</div>
                    <div className="col-md-6 text-md-right small">
                        <a href="#!">Privacy Policy</a>
                        &#xB7;
                        <a href="#!">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
</div>)
}

export default Error_404;
import React,{Component} from 'react';
import feather from 'feather-icons';
import { utility } from '../common/utility';
import {commonService} from '../common/service/CommonService';
import StorageService from '../common/service/StorageService';
import {GLOBAL_VARIABLE} from '../config/app.constants';
import Alert from 'react-s-alert';

class PermissionList extends Component{
    constructor(props) {
        super(props);
        this.storage = new StorageService();
    }
    state={
        pages : [],
        updateStatus: []
    }
    componentDidMount(){
        // Activate Feather icons
        feather.replace();

        commonService.getPermissionList(this.storage.getRoleIds(GLOBAL_VARIABLE.ROLE_ID)).then(page=>{
            this.setState({
                pages: page.data.permissionDetailList
            })
        }).catch(err=>console.log(err));        
    }

    handleCheckbox=(e)=>{
        let index = this.state.updateStatus.findIndex((obj => obj.id === e.target.id));
        if(index===-1){
            this.setState({
            ...this.state,
            updateStatus: [...this.state.updateStatus,{id:e.target.id, status: e.target.checked ? 'Y' :'N'}]
        })}else{
            utility.updateItemInArrObj(e.target.id,this,'updateStatus','id',{status: e.target.checked ? 'Y' :'N'})
        }
        
    }

    statusUpdate=()=>{
        if(this.state.updateStatus.length > 0){           
            this.state.updateStatus.forEach(val=>{
                commonService.updatePermissionList(val.id,val.status).then(res=>{                   
                   if(res.data.code==='200'){Alert.success('Status for each page has been updated.')};
                }).catch(err=>console.log(err)); 
            });            
            
        }else{
            Alert.info('Nothing is selected. No changes made.');
        }
    }

    render(){
        return(
            <main>
            <header className="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
                <div className="container">
                    <div className="page-header-content pt-4">
                        <div className="row align-items-center justify-content-between">
                            <div className="col-auto mt-6">
                                <h2 className="page-header-title">Permission List </h2>
                                                                    
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div className="container mt-n10">
                <div className="card mb-4">
                    <div className="card-header"> 
                                
                    <button className="btn btn-primary mr-2 my-1" type="button" onClick={this.statusUpdate}> Update </button>
                    
                    </div>
                    <div className="card-body">
                        <div className="datatable">
                            <table className="table table-bordered table-hover" id="dataTable" width="100%" cellSpacing="0">
                                <thead>
                                    <tr>
                                        <th> Category Type </th>
                                        <th> Functionality </th>
                                        <th> Page ID </th>
                                        <th> Page Name </th>
                                        <th> Role ID</th>
                                        <th> Role Name </th>
                                        <th> Status </th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    {
                                        this.state.pages.map(page=>{
                                            return(
                                        <tr key={page.id}>
                                            <td>{page.catType}</td>
                                            <td>{page.functionality}</td>
                                            <td>{page.pageId}</td>
                                            <td>{page.pageName}</td>
                                            <td>{page.roleId}</td>
                                            <td>{page.roleName}</td>
                                            <td>
                                                <div className="custom-control custom-checkbox">
                                                    <input className="custom-control-input custom-checkbox" id={page.id} type="checkbox" defaultChecked={ page.status ==='Y' ? true : false} onChange={this.handleCheckbox} />
                                                    <label className="custom-control-label" htmlFor={page.id}></label>
                                                </div>
                                            </td>
                                        </tr>)
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            </main>
        )
    }
}

export default PermissionList;
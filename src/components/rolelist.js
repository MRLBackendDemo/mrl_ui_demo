import React, { Component } from "react";
import feather from "feather-icons";
import { utility } from "../common/utility";
import { validations } from "../common/validations";
import { commonService } from "../common/service/CommonService";
import { Alert } from "../common/service/AlertService";

class RoleList extends Component {
  state = {
    roleList: [],
  };
  componentDidMount() {
    // Activate Feather icons
    feather.replace();
    //get all role list and display
    commonService
      .getRoleList()
      .then(role => {
        this.setState({
          roleList: role.data,
        });
      })
      .catch((err) => console.log(err));
  }

  formData = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  addRole = (e) => {
    
    e.preventDefault();
    let vals = this.state;
    if (validations.addRoleValidate(vals)) {
      commonService
        .addRole(vals.createDateTime, vals.roleName, vals.status)
        .then(res => {
          if (res.data.code === "200") {
              this.setState({
                ...vals,
                roleList: [
                    ...vals.roleList,{
                    roleId: res.data.roleId,
                    roleName:vals.roleName,
                    status:vals.status,
                    createDataTime:vals.createDateTime
                    }
                ]
              });
            document.querySelector("#addRoleForm").reset();
            document.querySelector(".addRoleFormClose").click();
            Alert.alertBar(
              `Role: ${vals.roleName} has been added.`,
              "bg-green"
            );
          }
        })
        .catch((err) => console.log(err));
    }
  };

  editUserRole = (e) =>{
    let vals = this.state;
    e.preventDefault();
    if (validations.updateRoleValidate(vals)) {
        commonService
        .updateRole( 
            vals.editRoleId,
            vals.editRoleDate,
            vals.editRoleName,
            vals.editRoleStatus)
        .then(res=>{
            if (res.data.code === "200") {
                utility.updateItemInArrObj(vals.editRoleId, this, 'roleList','roleId',{
                    roleName:vals.editRoleName,
                    status:vals.editRoleStatus,
                    createDataTime:vals.editRoleDate
                });
                document.querySelector("#updateRoleForm").reset();
                document.querySelector(".updateRoleFormClose").click();
                Alert.alertBar(
                  `Role: ${vals.editRoleName} has been updated.`,
                  'bg-green'
                );
              }
        })
        .catch(err=>console.log(err));

    }
  }
  populateEditTable = (e, id) => {   
    e = e || window.event;
    let data = [];
    let target = e.srcElement || e.target;
    while (target && target.nodeName !== "TR") {
      target = target.parentNode;
    }
    if (target) {
      let cells = target.getElementsByTagName("td");
      for (var i = 0; i < cells.length; i++) {
        data.push(cells[i].innerHTML);
      }
    }
    document.getElementById("editRoleName").value = data[0];
    document.getElementById("editRoleStatus").value = data[1];
    document.getElementById("editRoleDate").value = data[2];
    this.setState({
      ...this.state,
      editRoleId: id,
      editRoleDate: data[2],
      editRoleStatus: data[1],
      editRoleName: data[0]
    });
  };

  render() {
    return (
      <main>
        <header className="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
          <div className="container">
            <div className="page-header-content pt-4">
              <div className="row align-items-center justify-content-between">
                <div className="col-auto mt-6">
                  <h2 className="page-header-title">Role List </h2>
                </div>
              </div>
            </div>
          </div>
        </header>
        {/* <!-- Main page content--> */}
        <div className="container mt-n10">
          <div className="card mb-4">
            <div className="card-header">
              {/* <!-- Modal--> */}
              <div
                className="modal fade"
                id="updateUserRole"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="updateRoleTitle"
                aria-hidden="true"
              >
                <div
                  className="modal-dialog modal-dialog-scrollable-true"
                  role="document"
                >
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title" id="updateRoleTitle">
                        Role Edit
                      </h5>
                      <button
                        className="updateRoleFormClose close"
                        type="button"
                        data-dismiss="modal"
                        aria-label="Close"
                      >
                        <span aria-hidden="true">&#xD7;</span>
                      </button>
                    </div>
                    <form id="updateRoleForm" onSubmit={this.editUserRole}>
                    <div className="modal-body">
                      
                        {/* <!-- Form Row--> */}
                        <div className="form-row">
                          <div className="col-md-12">
                            <div className="form-group">
                              <label
                                className="text-gray-600 small"
                                htmlFor="editRoleName"
                              >
                                Role Name
                              </label>
                              <input
                                className="form-control form-control-solid py-4"
                                id="editRoleName"
                                type="text"
                                placeholder=""
                                aria-label="Role Name"
                                aria-describedby="lastNameExample"
                                onChange={this.formData}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="form-group">
                          <label
                            className="text-gray-600 small"
                            htmlFor="editRoleStatus"
                          >
                            Status
                          </label>
                          <select
                            className="form-control"
                            id="editRoleStatus"
                            onChange={this.formData}
                          >
                              <option>Select</option>
                              <option value="Y">Active </option>
                            <option value="I">Inactive</option>
                          </select>
                        </div>

                        {/* <!-- Form Row--> */}
                        <div className="form-row">
                          <div className="col-md-12">
                            {/* <!-- Form Group (choose password)--> */}
                            <div className="form-group">
                              <label
                                className="text-gray-600 small"
                                htmlFor="editRoleDate"
                              >
                                Create Date
                              </label>

                              <input
                                className="form-control form-control-solid py-4"
                                type="date"
                                id="editRoleDate"
                                aria-label="Date"
                                aria-describedby="dateExample"
                                onChange={this.formData}
                              />
                            </div>
                          </div>
                        </div>
                      
                    </div>
                    <div className="modal-footer">
                      <button
                        className="btn btn-success"
                        type="submit"                     
                      >
                        Update
                      </button>
                      <button className="btn btn-danger" type="button" data-dismiss="modal">
                        Cancel
                      </button>
                    </div>
                    </form>
                  </div>
                </div>
              </div>

              <button
                className="btn btn-primary mr-2 my-1"
                type="button"
                data-toggle="modal"
                data-target="#addnewRole"
              >
                <i data-feather="plus"></i>&nbsp;Add{" "}
              </button>
              {/* <!-- Modal--> */}

              <div
                className="modal fade"
                id="addnewRole"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="addnewRoleTitle"
                aria-hidden="true"
              >
                <div
                  className="modal-dialog modal-dialog-scrollable-true"
                  role="document"
                >
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5
                        className="modal-title"
                        id="addnewRoleTitle"
                      >
                        Role Add
                      </h5>
                      <button
                        className="addRoleFormClose close"
                        type="button"
                        data-dismiss="modal"
                        aria-label="Close"
                      >
                        <span aria-hidden="true">&#xD7;</span>
                      </button>
                    </div>
                    <form id="addRoleForm" onSubmit={this.addRole}>
                      <div className="modal-body">
                        {/* <!-- Form Row--> */}
                        <div className="form-row">
                          <div className="col-md-12">
                            <div className="form-group">
                              <label
                                className="text-gray-600 small"
                                htmlFor="roleNameExample"
                              >
                                Role Name
                              </label>
                              <input
                                className="form-control form-control-solid py-4"
                                type="text"
                                id="roleName"
                                placeholder=""
                                aria-label="Role Name"
                                aria-describedby="roleName"
                                onChange={this.formData}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="form-group">
                          <label
                            className="text-gray-600 small"
                            htmlFor="exampleFormControlSelect1"
                          >
                            Status
                          </label>
                          <select
                            className="form-control"
                            id="status"
                            onChange={this.formData}
                          >
                            <option>Select</option>
                            <option value="Y">Active </option>
                            <option value="I">Inactive</option>
                          </select>
                        </div>
                        {/* <!-- Form Row--> */}
                        <div className="form-row">
                          <div className="col-md-6">
                            {/* <!-- Form Group --> */}
                            <div className="form-group">
                              <label
                                className="text-gray-600 small"
                                htmlFor="createdate"
                              >
                                Create Date
                              </label>
                              <input
                                className="form-control form-control-solid py-4"
                                type="date"
                                id="createDateTime"
                                placeholder=""
                                aria-label="Date"
                                aria-describedby="dateExample"
                                onChange={this.formData}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="modal-footer">
                        <button className="btn btn-primary" type="submit">
                          {" "}
                          Add{" "}
                        </button>
                        <button
                          className="btn btn-danger"
                          type="button"
                          data-dismiss="modal"
                        >
                          Cancel
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div className="card-body">
              <div className="datatable">
                <table
                  className="table table-bordered table-hover roleListTable"
                  id="dataTable"
                  width="100%"
                  cellSpacing="0"
                >
                  <thead>
                    <tr>
                      <th>Role Name </th>
                      <th>Status</th>
                      <th>Start date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.roleList.map((role) => {
                      return (
                        <tr key={role.roleId}>
                          <td>{role.roleName}</td>
                          <td>{role.status}</td>
                          <td>{role.createDataTime}</td>
                          <td>
                            <button
                              className="btn btn-success"
                              type="button"
                              data-toggle="modal"
                              data-target="#updateUserRole"
                              onClick={(e) => {
                                this.populateEditTable(e, role.roleId);
                              }}
                            >
                              <i className="fas fa-edit"></i>&nbsp;Edit{" "}
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </main>
    );
  }
}

export default RoleList;

import React, { Component } from "react";
import feather from "feather-icons";

class MbqUploadSingle extends Component {
  componentDidMount() {
    feather.replace();
  }

  render() {
    return (
      <main>
        <header className="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
          <div className="container">
            <div className="page-header-content pt-4">
              <div className="row align-items-center justify-content-between">
                <div className="col-auto mt-6">
                  <h2 className="page-header-title"> MBQ Upload </h2>
                </div>
              </div>
            </div>
          </div>
        </header>
        <div className="container mt-n10">
          <div className="card mb-4">
            <div className="card-header">
              <div className="card-body">
                {/* <!-- Component Preview--> */}
                <form>
                  <div className="form-row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label htmlFor="exampleFormControlSelect1">Item</label>
                        <select
                          className="form-control"
                          id="exampleFormControlSelect1"
                        >
                          <option>Type Search </option>
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <label htmlFor="exampleFormControlSelect1">
                          Location
                        </label>
                        <select
                          className="form-control"
                          id="exampleFormControlSelect1"
                        >
                          <option>Type Search </option>
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <label htmlFor="exampleFormControlInput1">
                          Min. Stock{" "}
                        </label>
                        <input
                          className="form-control"
                          id="exampleFormControlInput1"
                          type="textbox"
                          placeholder="min stock"
                        />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <label htmlFor="exampleFormControlInput1">
                          Max. Stock{" "}
                        </label>
                        <input
                          className="form-control"
                          id="exampleFormControlInput1"
                          type="textbox"
                          placeholder="max stock"
                        />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <label htmlFor="Date">Future Date:</label>{" "}
                        <input
                          className="form-control"
                          type="date"
                          id="date"
                          name="future date"
                        />
                      </div>
                      <button className="btn btn-success btn-sm py-2 px-3" type="submit">
                        Update
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </main>
    );
  }
}

export default MbqUploadSingle;

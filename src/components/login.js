import React,{Component} from 'react';
import { Link } from 'react-router-dom';
import { validations } from '../common/validations';
import StorageService from '../common/service/StorageService';
import {GLOBAL_API, CONFIG} from '../config/app.constants';
import Alert from 'react-s-alert';
import axios from 'axios';

class Login extends Component{
    constructor(props) {
        super(props);
        this.storage = new StorageService();
    }
    state = {
        email: '',
        pass: ''
    }
    credentials = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        });
      }
    
      verifyLogin = (e) =>{
        e.preventDefault();
        if(validations.loginValidate(this.state)){
            if(validations.validateEmail(this.state.email)){
                axios.post(GLOBAL_API.LOGIN,{ userEmail : this.state.email,userPassword: this.state.pass },CONFIG)
                .then(res=>{                  
                    if(res.status===200){                        
                        if(res.data.code === "200"){
                          this.storage.setRoleIds(res.data.roleIds);
                            console.log('Login success, redirecting to dashboard...');
                            this.props.history.push('/dashboard');                  
                        }else{
                            Alert.error(`Error: ${res.data.message}.`);                
                        }
                    }else{Alert.error('Internal Server Error: Please try again Later.')};                
                }).catch(err=>console.log(err));
            }
        }
      }
      render(){
          return(
          <div className="mrl-bg-img" id = "layoutAuthentication">
          <div id = "layoutAuthentication_content">
            <main>
              <div className = "container">
                <div className="row justify-content-center">
                  <div className="col-xl-5 col-lg-6 col-md-8 col-sm-11">
                    <div className="card my-5">
                      <div className="card-body p-5 text-center">
                      <p className="card-body p-5 text-center mrl-logo"></p>
                      </div>
                      <hr className="my-0"/>
                      <div className="card-body p-5">
                        <form onSubmit={this.verifyLogin}>
                          <div className="form-group">
                            <label htmlFor ="email" className="text-gray-600 small" >User ID</label>
                            <input className="form-control form-control-solid py-4" type="text" id="email" placeholder="" aria-label="Email Address" aria-describedby="emailExample" onChange={this.credentials}/>
                          </div>
                          <div className="form-group">
                            <label htmlFor = "password" type="password" name="password" className="text-gray-600 small" >Password</label>
                            <input className="form-control form-control-solid py-4" type="password" id="pass" placeholder="" aria-label="Password" aria-describedby="passwordExample" onChange={this.credentials}/>
                          </div>
                          <div className="form-group">
                              <Link className="small" to="/forgotpassword">Forgot your password?</Link>
                          </div>
                          <div className="form-group d-flex align-items-center justify-content-between mb-0">
                            <div className="custom-control custom-control-solid custom-checkbox">
                             <input className="custom-control-input small" id="customCheck1" type="checkbox"/>
                             <label className="custom-control-label" >Remember password</label>
                            </div>
                           <button className="btn btn-warning" type="submit">Login</button>
                         </div>
                        </form>
                      </div>
                       <hr className="my-0"/>
                       <div className="card-body px-5 py-4">
                          <div className="small text-center">
                            New user?
                            <Link to="/register">Create an account!</Link>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </main>
          </div>
        </div>);
      }
}

export default Login;
import React, { Component } from "react";
import { utility } from "../common/utility";
import { validations } from "../common/validations";
import { Alert } from "../common/service/AlertService";
import { commonService } from "../common/service/CommonService";
import feather from "feather-icons";

class UserList extends Component {
  state = {
    userList: [],
  };
  componentDidMount() {
    // Activate Feather icons
    feather.replace();
    //get all user list and display
    commonService
      .getUserList()
      .then((user) => {
        this.setState({
          userList: user.data,
        });
      })
      .catch((err) => console.log(err));
  }

  formData = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  addUser = (e) => {
    let vals = this.state;
    e.preventDefault();
    if (validations.addUserValidate(vals)) {
      commonService
        .addUser(
          vals.createDateTime,
          vals.status,
          vals.userEmail,
          vals.userPassword
        )
        .then((res) => {
          if (res.data.code === "200") {
            this.setState({
              ...vals,
              userList: [
                ...vals.userList,
                {
                  createDateTime: vals.createDateTime,
                  status: vals.status,
                  userEmail: vals.userEmail,
                  userId: res.data.userId,
                  userPassword: vals.userPassword,
                  userRoleMappings: [{ roleIds: "" }],
                }]
            });
            document.querySelector("#addUserForm").reset();
            document.querySelector(".addUserFormClose").click();
            Alert.alertBar(
              `User: ${vals.userEmail} has been added.`,
              "bg-green"
            );
          }
        })
        .catch((err) => console.log(err));
    }
  };

  editUser = (e) => {
    let vals = this.state;
    e.preventDefault();
    if (validations.updateUserValidate(vals)) {
      commonService
        .updateUser(
          vals.editUserId,
          vals.editCreateDateTime,
          vals.editStatus,
          vals.editUserEmail,
          vals.editUserPassword
        )
        .then((res) => {
          if (res.data.code === "200") {
            utility.updateItemInArrObj(vals.editUserId, this, 'userList', 'userId',{
              createDateTime: vals.editCreateDateTime,
              status: vals.editStatus,
              userEmail: vals.editUserEmail,
              userPassword: vals.editUserPassword,
            });
            document.querySelector("#updateUserForm").reset();
            document.querySelector(".updateUserFormClose").click();
            Alert.alertBar(
              `User: ${vals.editUserEmail} has been updated.`,
              "bg-green"
            );
          }
        })
        .catch((err) => console.log(err));
    }
  };

  populateEditForm = (e, id) => {
    e = e || window.event;
    let data = [];
    let target = e.srcElement || e.target;
    while (target && target.nodeName !== "TR") {
      target = target.parentNode;
    }
    if (target) {
      let cells = target.getElementsByTagName("td");
      for (var i = 0; i < cells.length; i++) {
        data.push(cells[i].innerHTML);
      }
    }
    document.getElementById("editUserEmail").value = data[0];
    document.getElementById("editUserPassword").value = data[1];
    document.getElementById("editStatus").value = data[2];
    document.getElementById("editCreateDateTime").value = data[3];
    this.setState({
      ...this.state,
      editUserId: id,
      editCreateDateTime: data[3],
      editStatus: data[2],
      editUserEmail: data[0],
      editUserPassword: data[1],
    });
  };

  render() {
    return (
      <main>
        <header className="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
          <div className="container">
            <div className="page-header-content pt-4">
              <div className="row align-items-center justify-content-between">
                <div className="col-auto mt-6">
                  <h2 className="page-header-title">User List </h2>
                </div>
              </div>
            </div>
          </div>
        </header>
        {/* <!-- Main page content--> */}
        <div className="container mt-n10">
          <div className="card mb-4">
            <div className="card-header">
              {/* <!-- Edit user Modal--> */}
              <div
                className="modal fade"
                id="editUserModal"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="editUserModalForm"
                aria-hidden="true"
              >
                <div
                  className="modal-dialog modal-dialog-scrollable-true"
                  role="document"
                >
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5
                        className="modal-title"
                        id="exampleModalScrollableTitle"
                      >
                        User Edit
                      </h5>
                      <button
                        className="updateUserFormClose close"
                        type="button"
                        data-dismiss="modal"
                        aria-label="Close"
                      >
                        <span aria-hidden="true">&#xD7;</span>
                      </button>
                    </div>
                    <form id="updateUserForm" onSubmit={this.editUser}>
                      <div className="modal-body">
                        {/* <!-- Form Row--> */}
                        <div className="form-row">
                          <div className="col-md-12">
                            <div className="form-group">
                              <label
                                className="text-gray-600 small"
                                htmlFor="editUserEmail"
                              >
                                User Email
                              </label>
                              <input
                                className="form-control form-control-solid py-4"
                                id="editUserEmail"
                                type="text"
                                placeholder=""
                                aria-label="User Email"
                                aria-describedby="userEmail"
                                onChange={this.formData}
                              />
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-group">
                              <label
                                className="text-gray-600 small"
                                htmlFor="editUserPassword"
                              >
                                User Password
                              </label>
                              <input
                                className="form-control form-control-solid py-4"
                                type="password"
                                id="editUserPassword"
                                placeholder=""
                                aria-label="User Password"
                                aria-describedby="user's password"
                                onChange={this.formData}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="form-group">
                          <label
                            className="text-gray-600 small"
                            htmlFor="editStatus"
                          >
                            Status
                          </label>
                          <select
                            className="form-control"
                            id="editStatus"
                            onChange={this.formData}
                          >
                            <option>Select</option>
                            <option value="A">Active </option>
                            <option value="I">Inactive</option>
                          </select>
                        </div>

                        {/* <!-- Form Row--> */}
                        <div className="form-row">
                          <div className="col-md-12">
                            {/* <!-- Form Group--> */}
                            <div className="form-group">
                              <label
                                className="text-gray-600 small"
                                htmlFor="editCreateDateTime"
                              >
                                Create Date
                              </label>

                              <input
                                className="form-control form-control-solid py-4"
                                id="editCreateDateTime"
                                type="date"
                                placeholder=""
                                aria-label="Date"
                                aria-describedby="editUserCreateDate"
                                onChange={this.formData}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="modal-footer">
                        <button className="btn btn-success" type="submit">
                          Update
                        </button>
                        <button
                          className="btn btn-danger"
                          type="button"
                          data-dismiss="modal"
                        >
                          Cancel
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

              <button
                className="btn btn-primary mr-2 my-1"
                type="button"
                data-toggle="modal"
                data-target="#addUserModal"
              >
                <i data-feather="plus"></i>&nbsp;Add
              </button>
              {/* <!-- Add User Modal--> */}

              <div
                className="modal fade"
                id="addUserModal"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="addUserModalTitle"
                aria-hidden="true"
              >
                <div
                  className="modal-dialog modal-dialog-scrollable-true"
                  role="document"
                >
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title" id="addUserModalTitle">
                        User Add
                      </h5>
                      <button
                        className="addUserFormClose close"
                        type="button"
                        data-dismiss="modal"
                        aria-label="Close"
                      >
                        <span aria-hidden="true">&#xD7;</span>
                      </button>
                    </div>
                    <form id="addUserForm" onSubmit={this.addUser}>
                      <div className="modal-body">
                        {/* <!-- Form Row--> */}
                        <div className="form-row">
                          <div className="col-md-12">
                            <div className="form-group">
                              <label
                                className="text-gray-600 small"
                                htmlFor="userEmail"
                              >
                                User Mail
                              </label>
                              <input
                                className="form-control form-control-solid py-4"
                                type="text"
                                id="userEmail"
                                placeholder=""
                                aria-label="Role Name"
                                aria-describedby="lastNameExample"
                                onChange={this.formData}
                              />
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-group">
                              <label
                                className="text-gray-600 small"
                                htmlFor="userPassword"
                              >
                                User Password
                              </label>
                              <input
                                className="form-control form-control-solid py-4"
                                type="password"
                                id="userPassword"
                                placeholder=""
                                aria-label="User Password"
                                aria-describedby="user's password"
                                onChange={this.formData}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="form-group">
                          <label
                            className="text-gray-600 small"
                            htmlFor="exampleFormControlSelect1"
                          >
                            Status
                          </label>
                          <select
                            className="form-control"
                            id="status"
                            onChange={this.formData}
                          >
                            <option>Select</option>
                            <option value="A">Active </option>
                            <option value="I">Inactive</option>
                          </select>
                        </div>
                        {/* <!-- Form Row--> */}
                        <div className="form-row">
                          <div className="col-md-12">
                            {/* <!-- Form Group (choose password)--> */}
                            <div className="form-group">
                              <label
                                className="text-gray-600 small"
                                htmlFor="createdate"
                              >
                                Create Date
                              </label>

                              <input
                                className="form-control form-control-solid py-4"
                                type="date"
                                id="createDateTime"
                                placeholder=""
                                aria-label="Date"
                                aria-describedby="dateExample"
                                onChange={this.formData}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="modal-footer">
                        <button className="btn btn-primary" type="submit">
                          {" "}
                          Add{" "}
                        </button>
                        <button
                          className="btn btn-danger"
                          type="button"
                          data-dismiss="modal"
                        >
                          Cancel
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div className="card-body">
              <div className="datatable">
                <table
                  className="table table-bordered table-hover userListTable"
                  id="dataTable"
                  width="100%"
                  cellSpacing="0"
                >
                  <thead>
                    <tr>
                      <th>User Mail</th>
                      <th>Password </th>
                      <th> Status </th>
                      <th> Create Date </th>
                      <th>Role</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.userList.map((user) => {
                      return (
                        <tr key={Math.floor(Math.random() * 100000 + 1)}>
                          <td>{user.userEmail}</td>
                          <td>{user.userPassword}</td>
                          <td>{user.status}</td>
                          <td>{user.createDateTime}</td>
                          <td>
                            {user.userRoleMappings.map((rol) => {
                              return rol.roleIds + " ";
                            })}
                          </td>
                          <td>
                            <button
                              className="btn btn-success"
                              type="button"
                              data-toggle="modal"
                              data-target="#editUserModal"
                              onClick={(e) => {
                                this.populateEditForm(e, user.userId);
                              }}
                            >
                              <i className="fas fa-edit"></i>&nbsp;Edit
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </main>
    );
  }
}

export default UserList;

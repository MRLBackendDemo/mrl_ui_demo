import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import {commonService} from '../../common/service/CommonService';
import StorageService from '../../common/service/StorageService';
import { utility } from '../../common/utility';
import {GLOBAL_VARIABLE} from '../../config/app.constants';

class DashboardSidebar extends Component {
    constructor(props) {
        super(props);
        this.storage = new StorageService();
        this.cat_type = [];
        this.fun_type = [];        
    }
    state={
        permittedPages : []
    }    
    componentDidMount(){        
        commonService.getPermissionList(this.storage.getRoleIds(GLOBAL_VARIABLE.ROLE_ID)).then(page=>{ 
            if(this.storage.getRoleIds(GLOBAL_VARIABLE.ROLE_ID)==='0') {                  
                let catSet = new Set();
                page.data.pageDetailList.forEach(dat => catSet.add(dat.catType));
                this.cat_type=[...catSet];
                
                this.setState({
                    ...this.state,
                    permittedPages: page.data.pageDetailList
                })
                
            }else{              
                let catSet = new Set();
                page.data.permissionDetailList.forEach(dat => catSet.add(dat.catType));
                this.cat_type=[...catSet];                
                this.setState({
                    ...this.state,
                    permittedPages: utility.uniqueArray(page.data.permissionDetailList, 'pageId')
                })                
               
            }   
            
        }).catch(err=>console.log(err));        
    }
    funAdd=(cat)=>{
         let funSet = new Set();
            this.state.permittedPages.forEach(dat =>{
             if(cat===dat.catType){ funSet.add(dat.functionality);}
            });
            this.fun_type=[...funSet];
    }
   
    render() {       
        return (
            <div id="layoutSidenav_nav">
                <nav className="sidenav shadow-right sidenav-light">
                    <div className="sidenav-menu">
                        <div className="nav accordion" id="accordionSidenav">
                            <NavLink className="nav-link" to="/dashboard">
                                <div className="nav-link-icon"><i data-feather="activity"></i></div>
                                    Dashboards
                            </NavLink>
                            {this.cat_type.map((cat,index)=>{
                            return(
                        
                            <div key={index}>
                            <a className="nav-link collapsed" href="na" data-toggle="collapse" data-target={'#'+cat+index} aria-expanded="false" aria-controls={cat+index}>
                                <div className="nav-link-icon"><i className="fas fa-border-all"></i></div>
                                {utility.lowFstLetr(cat)}
                                <div className="sidenav-collapse-arrow"><i className="fas fa-angle-down"></i></div>
                            </a>
                            
                            <div className="collapse" id={cat+index} data-parent="#accordionSidenav">
                                <nav className="sidenav-menu-nested nav accordion" id="accordionSidenavPagesMenu">
                                {this.funAdd(cat)}
                                {
                                this.fun_type.map((fun,index)=>{
                                    return(
                                <div key={index}>
                                    <a className="nav-link collapsed" href="na"  data-toggle="collapse" data-target={'#'+fun+index} aria-expanded="false" aria-controls={fun+index}>
                                        <div className="nav-link-icon"><i className="far fa-stop-circle"></i> </div>  {utility.lowFstLetr(fun.replace(/_/g, ' '))}
                                            <div className="sidenav-collapse-arrow"><i className="fas fa-angle-down"></i></div>
                                    </a>
                                    <div className="collapse" id={fun+index} data-parent="#accordionSidenavPagesMenu">
                                        <nav className="sidenav-menu-nested nav">
                                            {this.state.permittedPages.map(page=>{ 
                                                return(  page.functionality === fun ? 
                                                <NavLink key={page.id+Math.floor((Math.random() * 100000) + 1)} className="nav-link" to={'/'+page.pageName.replace(/_/g, '')}><div className="nav-link-icon"><i className="far fa-file"></i></div>
                                                    {utility.capFstLetr(utility.lowFstLetr(page.pageName.replace(/_/g, ' ')))}                                                
                                                </NavLink>  : null
                                                )

                                            })
                                            }
                                        </nav>
                                    </div>
                                </div>)
                                })
                                }

                                </nav>
                            </div>
                            </div>
                            )
                            })
                            }                        
                        </div>
                    </div>
                    <div className="sidenav-footer">
                        <div className="sidenav-footer-content">
                            <div className="sidenav-footer-subtitle">Logged in as:</div>
                            <div className="sidenav-footer-title">ADMIN</div>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }

}

export default DashboardSidebar;
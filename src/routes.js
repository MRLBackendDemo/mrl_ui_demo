import React from 'react';
const Dashboard = React.lazy(() => import('./components/dashboard'));
const RoleList = React.lazy(() => import('./components/rolelist'));
const UserList = React.lazy(() => import('./components/userlist'));
const PermissionList = React.lazy(() => import('./components/permissionlist'));
const MbqUploadSingle = React.lazy(() => import('./components/mbqUploadSingle'));

const routes = [
    { path: '/', exact: true, name: 'Layout' },
    { path: '/dashboard', name: 'Dashboard', component: Dashboard },
    { path: '/rolelist', name: 'RoleList', component: RoleList },
    { path: '/userlist', name: 'UserList', component: UserList },
    { path: '/permissionlist', name: 'Permission', component: PermissionList },
    { path: '/mbqUploadSingle', name: 'MbqUploadSingle', component: MbqUploadSingle }
  ];
  
  export default routes;